# THIS FILE IS MANAGED BY PUPPET

# this is a clone of vpnred-service, but with the proto changed to tcp. Also,
# we don't run the server start and stop scripts, because it would be redundant.
#
# here are the changes:
#
#  - proto udp
#  + proto tcp-server
#  - server 172.27.0.0 255.255.252.0
#  + server 172.27.100.0 255.255.252.0
#  - push "dhcp-option DNS 172.27.0.1"
#  + push "dhcp-option DNS 172.27.100.1"
#  - status /var/run/openvpn-status 10
#  + status /var/run/openvpn-status-tcp 10
#  - management 127.0.0.1 1000
#  + management 127.0.0.1 1001
#  - up   /etc/openvpn/server-up.sh
#  - down /etc/openvpn/server-down.sh
#

##
## NETWORKING
## 

dev tun
proto tcp-server
topology subnet

##
## TIMEOUTS
##

keepalive 7 35
persist-key
persist-tun
persist-local-ip
persist-remote-ip

##
## SCRIPTS
##

#user nobody    \ well, this sucks, but the current setup needs to be able
#group nogroup  / to run ifconfig and iptables. so, for now, we run as root.

script-security 2
learn-address /etc/openvpn/vpn-route

##
## KEYS and CERTIFICATES
##

ca    /etc/certs/vpnred/ca/cert.pem
cert  /etc/certs/vpnred/vpn.riseup.net/cert.pem
key   /etc/certs/vpnred/vpn.riseup.net/key.pem
dh    /etc/certs/vpnred/dh1024.pem
cipher AES-256-CBC
auth SHA256
# must set in older openvpn to enable negotiation or it forces 1.0 only
# adjust this once we upgrade
tls-version-min 1.0

##
## SERVER MODE
##

server 172.27.100.0 255.255.252.0
push "redirect-gateway def1"
push "dhcp-option DNS 172.27.100.1"

##
## AUTHENTICATION
##

plugin /usr/lib/openvpn/openvpn-plugin-auth-pam.so openvpn
client-cert-not-required
username-as-common-name
duplicate-cn
client-config-dir /etc/openvpn/clients

##
## LOGGING
##

# there are many false positives of replay warning when over a high latency network,
# so we disable these warnings. If you think you are being SYN flooded, turn this back
# on to see the warnings. I think this is a UDP only thing and does not apply to
# TCP connections.
mute-replay-warnings

# limit repeat log items to five
mute 5

# increase this number to get more verbose logs
verb 2

# write out status every ten seconds
status /var/run/openvpn-status-tcp 10
status-version 3

# allow management console access
management 127.0.0.1 1001

# don't wait to aggregate packets
tcp-nodelay
