#!/usr/bin/perl
# -*- perl -*-

=head1 NAME

openvpn - Plugin to monitor number of users connected to openvpn server

=head1 USAGE

To use this plugin, add the following text to your openvpn server
configuration file:

 status /var/log/openvpn.status
 status-version 1

If you change the path to the status file, remember to change
env.statusfile in the plugin configuration to match the new path.

=head1 CONFIGURATION

This script uses the following configuration variables

 [openvpn]
  env.statusfileudp <path to status log file>
  env.statusfiletcp <path to status log file>

=head2 DEFAULT CONFIGURATION

The default configuration is

 [openvpn]
  env.statusfileudp /var/run/openvpn-status
  env.statusfiletcp /var/run/openvpn-status-tcp

=head1 AUTHOR

Based on original version by:
Copyright (C) 2005 Rodolphe Quiedeville <rodolphe@quiedeville.org>

=head1 LICENSE

Gnu GPLv2

=begin comment

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; version 2 dated June,
1991.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

If you improve this script please send your version to my email address
with the copyright notice upgrade with your name.

=end comment

=head1 MAGIC MARKERS

 #%# family=contrib
 #%# capabilities=autoconf

=cut

use strict;

my $statuslogfileudp = $ENV{'statusfileudp'} || '/var/run/openvpn-status';
my $statuslogfiletcp = $ENV{'statusfiletcp'} || '/var/run/openvpn-status-tcp';
my $usersudp = 0;
my $userstcp = 0;
my $total = 0;

if($ARGV[0] and $ARGV[0] eq "autoconf" ) {
    if(-f $statuslogfileudp) {
	if(-r $statuslogfileudp) {
	    print "yes\n";
	    exit 0;
	} else {
	    print "no (logfile not readable)\n";
	}
    } else {
	print "no (logfile not found)\n";
    }
    exit 0;
}

if ($ARGV[0] and $ARGV[0] eq "config" ){
    print "graph_title OpenVPN\n";
    print "graph_args --base 1000 -l 0\n";
    print "graph_scale yes\n";
    print "graph_vlabel users\n";
    print "graph_category vpn\n";
    print "graph_info This graph shows the numbers of users connected to openvpn server.\n";
    print "connections.label total users\n";
    print "connections.info The number of users connected to openvpn server\n";
    print "connections.draw LINE1\n";
    print "connectionsudp.label users(udp)\n";
    print "connectionsudp.info The number of users connected to openvpn server(udp)\n";
    print "connectionsudp.draw AREA\n";
    print "connectionstcp.label users(tcp)\n";
    print "connectionstcp.info The number of users connected to openvpn server(tcp)\n";
    print "connectionstcp.draw STACK\n";
    exit 0;
}

if (-f "$statuslogfileudp") {
    open(IN, "$statuslogfileudp") or exit 4;
    while(<IN>) {
        if (/^CLIENT_LIST/) {
            $usersudp = $usersudp + 1;
        }
    }
    close(IN);
}

if (-f "$statuslogfiletcp") {
    open(IN, "$statuslogfiletcp") or exit 4;
    while(<IN>) {
        if (/^CLIENT_LIST/) {
            $userstcp = $userstcp + 1;
        }
    }
    close(IN);
}

$total = $usersudp + $userstcp;

print "connections.value " . $total."\n";
print "connectionsudp.value " . $usersudp."\n";
print "connectionstcp.value " . $userstcp."\n";

