##
## This script takes various domain blocking lists, combines them, and converts them into a bind config.
##
## Requirements:
##
##   ruby, rubygems
##   gem install public-suffix-list
##

require 'rubygems'
require 'json'
require 'public_suffix_list'

$data_dir = 'block-list-sources'

#
# typically, if a domain shows up on one of our block lists, we shorten it
# to include only the first subdomain. this has the effect of blocking everything
# after the first subdomain if any subdomain shows up on a block list.
#
# for example:
# if the domain x.y.ads.example.co.uk appears somewhere in a block list, then
# we would normally block ads.example.co.uk and all subsequent subdomains.
#
# sometimes, however, this is not the desired behavior. this happens when one
# subdomain should be blocked, but sibling subdomains should not be blocked.
# typically, this is the case with CDNs where some CDN subdomains are for
# ad networks and other subdomains on the same CDN are for css.
#
# for example: ads.wikia.nocookie.net is in the blocklist, but we don't want
# this to also block images.wikia.nocookie.net.
#
# these domains WILL NOT GET BLOCKED, unless a subdomain is specifically
# found in a block list.
#
$subdomain_exceptions = %w(
  l.google.com video.google.com
  s3.amazonaws.com
  tripod.lycos.de tripod.lycos.es tripod.lycos.it tripod.lycos.nl tripod.lycos.se
  web.aol.com web.cs.com
  ak.facebook.com
  com.edgesuite.net de.edgesuite.net net.edgesuite.net g.akamai.net
  wikia.nocookie.net
  www.ibm.com
)

def get_domains

  domains = {}

  ##
  ## hosts-file.net
  ##

  # hosts-file.net is more than just ads, includes phisers.
  # it is a LOT of domains. For now, we just use the hosts-file.net ad server list.

  hosts = load_text_file 'ad-servers.txt'
  hosts.each do |domain|
    ip, domain = domain.split(/\s/)
    if ip == '127.0.0.1' and domain != 'localhost'
      domains[ cleanup_domain(domain) ] = true
    end
  end

  ##
  ## TACO
  ##

  taco = load_csv_file 'taco.list', 1
  taco.each do |domain|
    domains[ cleanup_domain(domain) ] = true
  end

  ##
  ## ABINE rule list
  ## e.g. '/home/elijah/.mozilla/firefox/cy6968xk.default/extensions/optout@dubfire.net/files/ui/scripts/tracker/rules.js'
  ## this rule list is interesting, because it includes mostly analytics trackers
  ##

  abine = load_abine_rules 'abine-rules.js'
  abine.each do |rule|
    domains[ cleanup_domain(rule) ] = true
  end

  ##
  ## yoyo.org
  ##

  yoyo = load_text_file 'yoyo.list'
  yoyo.each do |domain|
    domains[ cleanup_domain(domain) ] = true
  end

  # DISABLED
  ###
  ### industry list of opt-out cookies
  ### (taken from google's opt-out extension)
  ###
  #
  #registry = JSON.parse(File.read('chrome-opt-out-registry.json'))
  #registry['registry'].each do |ad_network|
  #  # remove leading '.'
  #  host = ad_network['host'].sub(/^\./, '')
  #  domains[ host.downcase ] = true
  #end

  ##
  ## adblock+ format lists
  ##

  #abp = load_text_file 'block-list-sources/Adversity.txt'
  #abp.each do |line|
  #  domains[ cleanup_domain(domain) ] = true
  #end

  ##
  ## custom lists
  ##

  allowed = load_text_file 'custom_allow_list.txt'
  blocked = load_text_file 'custom_block_list.txt'

  allowed_patterns = []
  allowed.each do |domain|
    next if domain.empty?
    ## e.g. *.facebook.com --> /^(.*\.)?facebook\.com/
    allowed_patterns << '^' + domain.gsub('.','\\.').gsub('*\\.', '(.*\\.)?')
  end
  domains.delete_if do |domain,value|
    allowed_patterns.detect{|pattern| domain =~ /#{pattern}/ }
  end

  blocked.each do |domain|
    domains[domain] = true
  end

  # sanity check, removes some garbage
  domains.each do |domain, dummy|
    next unless domain
    if domain.length < 4
      domains.delete(domain)
    end
  end

  return domains
end


##
## Functions
##

def load_text_file(filename)
  File.readlines(filename).collect do |line|
    line = line.strip
    if line =~ /[#<>]/
      ""
    else
      line
    end
  end
end

#def load_adblockplus_file(filename)
#  File.readlines(filename).collect do |line|
#    line = line.strip
#    if line =~ /^[!\/\&\.\-\?\_]/
#      nil
#    else
#      line
#    end
#  end
#end


def load_csv_file(filename, field_num)
  File.readlines(filename)[1..-1].collect do |line|
    line.strip.split(',')[field_num].gsub('"','')
  end
end

def load_abine_rules(rules_file)
  rules = File.read(rules_file)
  rules.scan(/(\{name:.*?\})/).collect do |rule|
    rule = rule.first
    name, category, pattern = rule.scan(/name:(.*?),category:(.*?),pattern:\/(.*?)\/i/).first
    pattern.gsub(/\\/,'') if pattern
  end.compact
end

def cleanup_domain(domain)
  return nil unless domain.chars.any?

  domain = domain.downcase

  # remove leading '.'
  domain = domain.sub(/^\./,'')

  # remove leading 'www'
  domain = domain.sub(/^www\./,'')

  # remove trailing path
  domain = domain.sub(/\/.*$/,'')

  # remove trailing dot
  domain = domain.sub(/\.$/,'')

  # skip entries that have a * in it, or ends with .js
  # (these come from abine rule patterns that don't have domains
  if domain =~ /\*|\.js$/
    puts 'skipping ' + domain
    return nil
  end

  # skip entries that don't end in a public suffix
  if $psl.tld(domain) == ""
    puts 'skipping ' + domain
    return nil
  end

  # allow one level deep subdomains after canonical domain name, then drop everything else.
  # ie, y.x.ads.adhost.co.uk -> ads.adhost.co.uk
  subdomains, cdn, tld = $psl.split(domain)
  if subdomains != ''
    shortened_domain = [subdomains.split('.').last, cdn, tld].join '.'
    if !$subdomain_exceptions.include?(shortened_domain)
      domain = shortened_domain
    end
  end

  return domain
end

##
## RUN IT
##

domains = nil

Dir.chdir($data_dir) do
  $psl = PublicSuffixList.new(:url => 'effective_tld_names.dat')
  domains = get_domains()
end

File.open('adblock.conf', 'w') do |f|
  f.puts '// AUTOGENERATED BY generate-adblock-conf.rb'
  domains.keys.compact.sort.each do |domain|
    f.puts 'zone "%s" { type master; notify no; file "/etc/bind/null.zone.file"; };' % domain
  end
end

