#!/bin/sh
# THIS FILE IS MANAGED BY PUPPET

#
# This script needs to be run at least once when the vpn server is started.
#

#
# remove existing rules if they already exist
#

iptables -t nat -D PREROUTING -p udp --dst <%= @ipaddress %> --dport 443 -j REDIRECT --to-ports 1194  2>/dev/null
iptables -t nat -D PREROUTING -p udp --dst <%= @ipaddress %> --dport 80  -j REDIRECT --to-ports 1194  2>/dev/null
iptables -t nat -D PREROUTING -p udp --dst <%= @ipaddress %> --dport 53  -j REDIRECT --to-ports 1194  2>/dev/null

iptables -t nat -D PREROUTING -p tcp --dst <%= @ipaddress %> --dport 443 -j REDIRECT --to-ports 1194  2>/dev/null
iptables -t nat -D PREROUTING -p tcp --dst <%= @ipaddress %> --dport 80  -j REDIRECT --to-ports 1194  2>/dev/null
iptables -t nat -D PREROUTING -p tcp --dst <%= @ipaddress %> --dport 53  -j REDIRECT --to-ports 1194  2>/dev/null

#
# remap incoming packets on ports 443, 80, 53 to port 1194.
# this is to help get past restrictive firewalls
#

iptables -t nat -A PREROUTING -p udp --dst <%= @ipaddress %> --dport 443 -j REDIRECT --to-ports 1194
iptables -t nat -A PREROUTING -p udp --dst <%= @ipaddress %> --dport 80  -j REDIRECT --to-ports 1194
iptables -t nat -A PREROUTING -p udp --dst <%= @ipaddress %> --dport 53  -j REDIRECT --to-ports 1194

iptables -t nat -A PREROUTING -p tcp --dst <%= @ipaddress %> --dport 443 -j REDIRECT --to-ports 1194
iptables -t nat -A PREROUTING -p tcp --dst <%= @ipaddress %> --dport 80  -j REDIRECT --to-ports 1194
iptables -t nat -A PREROUTING -p tcp --dst <%= @ipaddress %> --dport 53  -j REDIRECT --to-ports 1194

#
# ensure ip forwarding is enabled...
#

echo '1' > /proc/sys/net/ipv4/ip_forward

